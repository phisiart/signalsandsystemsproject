import cv2
import numpy
from matplotlib import pyplot

__author__ = 'Johnson Tan'

def split(src, plot=False):
    gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
    height = len(gray)
    width = len(gray[0])

    row_vars = [numpy.var(column) for column in gray]
    col_vars = [numpy.var(gray[:, row]) for row in range(0, width)]

    if plot:
        pyplot.figure()
        pyplot.plot(range(0, len(col_vars)), col_vars)

    if plot:
        pyplot.figure()
        pyplot.plot(range(0, len(row_vars)), row_vars)

    row_fft = numpy.abs(numpy.fft.fft(row_vars))[0:20]
    col_fft = numpy.abs(numpy.fft.fft(col_vars))[0:20]

    if plot:
        pyplot.figure()
        pyplot.plot(row_fft, '.')

    if plot:
        pyplot.figure()
        pyplot.plot(col_fft, '.')

    row_fft[0] = 0
    col_fft[0] = 0

    num_rows = numpy.argmax(row_fft)
    num_cols = numpy.argmax(col_fft)

    print 'number of rows :', num_rows
    print 'number of cols :', num_cols

    return (num_rows, num_cols)

def GetSize(src):
    height = len(src)
    width = len(src[0])
    return (height, width)

def GetBlock(gray, row_idx, col_idx, block_height, block_width):
    return gray[block_height * row_idx: block_height * (row_idx + 1), block_width * col_idx: block_width * (col_idx + 1)]

def GetLen(mat):
    ret = 0.0
    for row in mat:
        for col in row:
            ret += float(col)**2
    return ret**0.5

def GetDot(smaller, bigger):
    ret = 0.0
    for i in range(0, len(smaller)):
        for j in range(0, len(smaller[0])):
            ret += float(smaller[i][j]) * float(bigger[i][j])
    return ret



def GetCorr(mat1, mat2):
    return GetDot(mat1, mat2) / GetLen(mat1) / GetLen(mat2)

def GetShiftedCorr(mat1, mat2, x, y):
    mat1_ = ShiftUp(ShiftLeft(mat1, x), y)
    mat2_ = ShiftUp(ShiftLeft(mat2, -x), -y)
    return GetCorr(mat1_, mat2_)

def PrintShiftedCorrs(mat1, mat2, x_m, y_m):
    for x in range(-x_m, x_m):
        for y in range(-y_m, y_m):
            print x, y, GetShiftedCorr(mat1, mat2, x, y)

def ShiftLeft(mat, x):
    height, width = GetSize(mat)
    if x > 0:
        return mat[:,x:]
    else:
        return mat[:,:width + x]

def ShiftUp(mat, y):
    height, width = GetSize(mat)
    if y > 0:
        return mat[y:,:]
    else:
        return mat[: height + y,:]

def PrintAllCorr(gray, block_height, block_width, num_rows, num_cols):
    corrs = []
    for row0 in range(0, num_rows):
        for col0 in range(0, num_cols):
            block0 = GetBlock(gray, row0, col0, block_height, block_width)
            for row1 in range(0, num_rows):
                for col1 in range(0, num_cols):
                    block1 = GetBlock(gray, row1, col1, block_height, block_width)
                    # corr = GetDot(block0, block1) / GetLen(block0) / GetLen(block1)
                    corr = GetCorr(block0, block1)
                    corrs.append(corr)
                    if corr > 0.99:
                        print '(', row0, ',', col0, '), (', row1, ',', col1, ') : ', corr
    pyplot.figure()
    pyplot.plot(range(0, len(corrs)), corrs)
    pyplot.show()

if __name__ == '__main__':
    file_name = 'img.bmp'
    src = cv2.imread(file_name)
    gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
    num_rows, num_cols = split(src)

    height, width = GetSize(src)
    print height, width

    row_idx = 0
    col_idx = 0
    block_height = height / num_rows
    block_width = width / num_cols
    # block = gray[block_height * row_idx: block_height * (row_idx + 1), block_width * col_idx: block_width * (col_idx + 1)]

    PrintAllCorr(gray, block_height, block_width, num_rows, num_cols)

    block11 = GetBlock(gray, 1, 1, block_height, block_width)
    cv2.imshow('block00', block11)
    block24 = GetBlock(gray, 2, 4, block_height, block_width)
    cv2.imshow('block12', block24)

    bheight, bwidth = GetSize(block11)
    print 'block11 size :', bheight, bwidth

    bheight, bwidth = GetSize(block24)
    print 'block24 size :', bheight, bwidth

    print GetLen(block11)
    print GetLen(block24)
    print GetDot(block11, block24) / GetLen(block24) / GetLen(block11)

    # cropped = CropBottomRight(src, 0, 50)

    # cv2.imshow('src', src)
    # cv2.imshow('src cropped', cropped)
    #
    # PrintAllCorr(gray, block_height, block_width, num_rows, num_cols)
    #
    # shifted1 = cv2.imread('shifted1.bmp')
    # shifted1 = cv2.cvtColor(shifted1, cv2.COLOR_BGR2GRAY)
    # shifted2 = cv2.imread('shifted2.bmp')
    # shifted2 = cv2.cvtColor(shifted2, cv2.COLOR_BGR2GRAY)
    # PrintShiftedCorrs(shifted1, shifted2, 3, 3)

    cv2.waitKey()
