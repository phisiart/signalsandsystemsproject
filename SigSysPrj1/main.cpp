#include <iostream>
#include <string>
#include <tuple>
#include <vector>
#include "BMPFile.h"
#include "DFSet.h"
#include <complex>

using std::string;
using std::vector;
typedef std::complex<double> cdouble;

vector<cdouble> DTFT(vector<double> in) {
    vector<cdouble> ret;
    int N = in.size();
    double pi = 3.1415926;
    for (int i = 0; i < in.size(); ++i) {
        cdouble X;
        for (int n = 0; n < in.size(); ++n) {
            X += std::exp(cdouble(0.0, -2 * pi * i * n / N)) * double(in[n]);
        }
        ret.push_back(X);
    }
    return ret;
}

double Var(vector<int> in) {
    double sum = 0.0;
    double sumsquare = 0.0;
    for (int idx = 0; idx < in.size(); ++idx) {
        sum += in[idx];
        sumsquare += in[idx] * in[idx];
    }
    return sumsquare - sum * sum;
}

vector<double> DTFTabs(vector<double> in) {
    vector<cdouble> dtft = DTFT(in);
    vector<double> ret;
    for (auto x : dtft) {
        ret.push_back(std::abs(x));
    }
    return ret;
}

int ArgMax(vector<double> sequence) {
    double max = 0.0;
    int argmax = 1;
    for (int idx = 1; idx < sequence.size() / 2; ++idx) {
        if (sequence[idx] > max) {
            max = sequence[idx];
            argmax = idx;
        }
    }
    return argmax;
}

std::pair<int, int> Split(int** gray, int height, int width) {
    vector<vector<int> > cols;
    for (int colIdx = 0; colIdx < width; ++colIdx) {
        vector<int> col;
        for (int rowIdx = 0; rowIdx < height; ++rowIdx) {
            col.push_back(gray[rowIdx][colIdx]);
        }
        cols.push_back(col);
    }

    vector<vector<int> > rows;
    for (int rowIdx = 0; rowIdx < height; ++rowIdx) {
        vector<int> row;
        for (int colIdx = 0; colIdx < width; ++colIdx) {
            row.push_back(gray[rowIdx][colIdx]);
        }
        rows.push_back(row);
    }

    vector<double> colvars;
    for (int colIdx = 0; colIdx < width; ++colIdx) {
        auto& col = cols[colIdx];
        double var = Var(col);
        colvars.push_back(var);
    }
    auto dtftcol = DTFTabs(colvars);
    auto colmax = ArgMax(dtftcol);

    vector<double> rowvars;
    for (int rowIdx = 0; rowIdx < height; ++rowIdx) {
        auto& row = rows[rowIdx];
        double var = Var(row);
        rowvars.push_back(var);
    }
    auto dtftrow = DTFTabs(rowvars);
    auto rowmax = ArgMax(dtftrow);

    

    return std::make_pair(colmax, rowmax);
}

class Mat {
public:
    Pixel** pixels;
    int height;
    int width;

    Mat() : pixels(nullptr) {}

    Mat(Mat&& mat) : height(mat.height), width(mat.width), pixels(mat.pixels) {
        mat.pixels = nullptr;
    }

    Mat(const Mat& mat) : height(mat.height), width(mat.width) {
        pixels = new Pixel*[height];
        for (int rowIdx = 0; rowIdx < height; ++rowIdx) {
            pixels[rowIdx] = new Pixel[width];
            for (int colIdx = 0; colIdx < width; ++colIdx) {
                pixels[rowIdx][colIdx] = mat.pixels[rowIdx][colIdx];
            }
        }
    }

    ~Mat() {
        if (pixels == nullptr) {
            return;
        }
        for (int rowIdx = 0; rowIdx < height; ++rowIdx) {
            delete pixels[rowIdx];
        }
        delete pixels;
    }
};

Mat GetBlock(BMPImage image, int row, int col, int num_rows, int num_cols) {
    int width = image.Width / num_cols;
    int height = image.Height / num_rows;
    Mat mat;
    mat.width = width;
    mat.height = height;
    mat.pixels = new Pixel*[height];
    for (int rowIdx = 0; rowIdx < height; ++rowIdx) {
        mat.pixels[rowIdx] = new Pixel[width];
        for (int colIdx = 0; colIdx < width; ++colIdx) {
            mat.pixels[rowIdx][colIdx] = image.Image[row * height + rowIdx][col * width + colIdx];
        }
    }
    return mat;
}

Mat ShiftLeft(Mat in, int x) {
    Mat ret;
    int d = x;
    if (d < 0) {
        d = -d;
    }
    ret.width = in.width - d;
    ret.height = in.height;
    ret.pixels = new Pixel*[ret.height];
    
    if (x > 0) {
        for (int rowIdx = 0; rowIdx < ret.height; ++rowIdx) {
            ret.pixels[rowIdx] = new Pixel[ret.width];
            for (int colIdx = 0; colIdx < ret.width; ++colIdx) {
                ret.pixels[rowIdx][colIdx] = in.pixels[rowIdx][colIdx + x];
            }
        }
    } else {
        for (int rowIdx = 0; rowIdx < ret.height; ++rowIdx) {
            ret.pixels[rowIdx] = new Pixel[ret.width];
            for (int colIdx = 0; colIdx < ret.width; ++colIdx) {
                ret.pixels[rowIdx][colIdx] = in.pixels[rowIdx][colIdx];
            }
        }
    }
    return ret;
}

Mat ShiftUp(Mat in, int y) {
    Mat ret;
    int d = y;
    if (d < 0) {
        d = -d;
    }
    ret.width = in.width;
    ret.height = in.height - d;
    ret.pixels = new Pixel*[ret.height];

    if (y > 0) {
        for (int rowIdx = 0; rowIdx < ret.height; ++rowIdx) {
            ret.pixels[rowIdx] = new Pixel[ret.width];
            for (int colIdx = 0; colIdx < ret.width; ++colIdx) {
                ret.pixels[rowIdx][colIdx] = in.pixels[rowIdx + y][colIdx];
            }
        }
    } else {
        for (int rowIdx = 0; rowIdx < ret.height; ++rowIdx) {
            ret.pixels[rowIdx] = new Pixel[ret.width];
            for (int colIdx = 0; colIdx < ret.width; ++colIdx) {
                ret.pixels[rowIdx][colIdx] = in.pixels[rowIdx][colIdx];
            }
        }
    }
    return ret;
}

std::tuple<double, double, double> GetNorm(Mat mat) {
    double sum_r = 0.0;
    double sum_r2 = 0.0;
    double sum_g = 0.0;
    double sum_g2 = 0.0;
    double sum_b = 0.0;
    double sum_b2 = 0.0;

    for (int rowIdx = 0; rowIdx < mat.height; ++rowIdx) {
        for (int colIdx = 0; colIdx < mat.width; ++colIdx) {
            sum_r += mat.pixels[rowIdx][colIdx].R;
            sum_r2 += mat.pixels[rowIdx][colIdx].R * mat.pixels[rowIdx][colIdx].R;

            sum_g += mat.pixels[rowIdx][colIdx].G;
            sum_g2 += mat.pixels[rowIdx][colIdx].G * mat.pixels[rowIdx][colIdx].G;

            sum_b += mat.pixels[rowIdx][colIdx].B;
            sum_b2 += mat.pixels[rowIdx][colIdx].B * mat.pixels[rowIdx][colIdx].B;

        }
    }

    return std::make_tuple(std::sqrt(sum_r2), std::sqrt(sum_g2), std::sqrt(sum_b2));
}

std::tuple<double, double, double> GetDot(Mat mat1, Mat mat2) {
    double sum_r = 0.0;
    double sum_g = 0.0;
    double sum_b = 0.0;

    for (int rowIdx = 0; rowIdx < mat1.height; ++rowIdx) {
        for (int colIdx = 0; colIdx < mat1.width; ++colIdx) {
            sum_r += mat1.pixels[rowIdx][colIdx].R * mat2.pixels[rowIdx][colIdx].R;
            sum_g += mat1.pixels[rowIdx][colIdx].G * mat2.pixels[rowIdx][colIdx].G;
            sum_b += mat1.pixels[rowIdx][colIdx].B * mat2.pixels[rowIdx][colIdx].B;

        }
    }

    return std::make_tuple(sum_r, sum_g, sum_b);

}

std::tuple<double, double, double> GetCorr(Mat mat1, Mat mat2) {
    double dot_r, dot_g, dot_b;
    std::tie(dot_r, dot_g, dot_b) = GetDot(mat1, mat2);

    double norm_r1, norm_g1, norm_b1;
    double norm_r2, norm_g2, norm_b2;
    std::tie(norm_r1, norm_g1, norm_b1) = GetNorm(mat1);
    std::tie(norm_r2, norm_g2, norm_b2) = GetNorm(mat2);
    return std::make_tuple(dot_r / norm_r1 / norm_r2, dot_g / norm_g1 / norm_g2, dot_b / norm_b1 / norm_b2);
}

std::tuple<double, double, double> GetBestCorr(Mat mat1, Mat mat2, int x_m, int y_m) {
    double max = 0.0;
    double max_r = 0.0, max_g = 0.0, max_b = 0.0;
    for (int x = -x_m; x <= x_m; ++x) {
        for (int y = -y_m; y < y_m; ++y) {
            Mat shifted1 = ShiftLeft(ShiftUp(mat1, y), x);
            Mat shifted2 = ShiftLeft(ShiftUp(mat2, -y), -x);
            double r, g, b;
            std::tie(r, g, b) = GetCorr(shifted1, shifted2);
            if (std::sqrt(r*r + g*g + b*b) > max) {
                max = std::sqrt(r*r + g*g + b*b);
                max_r = r;
                max_g = g;
                max_b = b;
            }
        }
    }
    return std::make_tuple(max_r, max_g, max_b);
}

//
//  请勿随意修改SaveResult()函数
//  SaveResult函数保证了输出结果的格式符合我们测试的要求
//
void SaveResult(int SpeciesNumber, int RowNumber, int ColumnNumber, int **SpeciesMatrix, std::string StudentID) {
    std::ofstream fout;
    std::string file_name = StudentID;
    file_name += ".txt";
    //char *filename = new char[100];
    //strcpy(filename, StudentID);
    //strcat(filename, ".txt");
    fout.open(file_name);

    fout << "有几种小精灵?" << SpeciesNumber << std::endl;
    fout << "有几行小精灵?" << RowNumber << std::endl;
    fout << "有几列小精灵?" << ColumnNumber << std::endl;
    fout << "小精灵的位置分布:" << std::endl;

    if (SpeciesMatrix != nullptr) {
        for (int row = 0; row < RowNumber; row++) {
            for (int col = 0; col < ColumnNumber; col++) {
                fout << SpeciesMatrix[row][col];
                if (col != ColumnNumber - 1)
                    fout << "\t";
            }
            fout << std::endl;
        }
    }

    fout.close();
}


int main(int argc, char* argv[]) {
    string studentID = "2012011120";
    std::cout << studentID << std::endl;

    if (argc != 2) {
        std::cout << "Argument error! Please use the image file as the argument." << std::endl;
        return 1;
    }

    std::string imagePath = argv[1];
    //std::string imagePath = "img.bmp";
    BMPImage image;
    if (!BMPFile::read(imagePath.c_str(), image)) {
        std::cout << "Load image fail." << std::endl;
        return 1;
    }

    int** gray = image.rgb2Gray();
    int height = image.Height;
    int width = image.Width;

    int num_cols, num_rows;
    std::tie(num_cols, num_rows) = Split(gray, height, width);

    std::cout << num_cols << std::endl;
    std::cout << num_rows << std::endl;

    disjoinset dfs(num_cols * num_rows);
    for (int row1 = 0; row1 < num_rows; ++row1) {
        for (int col1 = 0; col1 < num_cols; ++col1) {
            Mat mat1 = GetBlock(image, row1, col1, num_rows, num_cols);
            for (int row2 = 0; row2 < num_rows; ++row2) {
                for (int col2 = 0; col2 < num_cols; ++col2) {
                    Mat mat2 = GetBlock(image, row2, col2, num_rows, num_cols);
                    double norm_r, norm_g, norm_b;
                    std::tie(norm_r, norm_g, norm_b) = GetBestCorr(mat1, mat2, 2, 2);
                    // std::cout << "(" << row1 << ", " << col1 << "), (" << row2 << ", " << col2 << ") : " << norm_r << ", " << norm_g << ", " << norm_b << std::endl;
                    if (norm_r > 0.99 && norm_g > 0.99 & norm_b > 0.99) {
                        dfs.union_set(row1 * num_cols + col1, row2 * num_cols + col2);
                    }
                }
            }
        }
    }

    int** species = new int*[num_rows];
    for (int row = 0; row < num_rows; ++row) {
        species[row] = new int[num_cols];
    }

    auto sets = dfs.get_sets();
    for (int setIdx = 0; setIdx < sets.size(); ++setIdx) {
        auto set = sets[setIdx];
        for (int item : set) {
            int col = item % num_cols;
            int row = item / num_cols;
            species[row][col] = setIdx + 1;
            // std::cout << item << "\t";
        }
        // std::cout << std::endl;
    }
    for (int row = 0; row < num_rows; row++) {
        for (int col = 0; col < num_cols; col++) {
            std::cout << species[row][col];
            if (col != num_cols - 1)
                std::cout << "\t";
        }
        std::cout << std::endl;
    }

    SaveResult(sets.size(), num_rows, num_cols, species, studentID.c_str());


    //BMPImage shifted1;
    //BMPFile::read("shifted1.bmp", shifted1);
    //BMPImage shifted2;
    //BMPFile::read("shifted2.bmp", shifted2);
    //Mat block1 = GetBlock(shifted1, 0, 0, 1, 1);
    //Mat block2 = GetBlock(shifted2, 0, 0, 1, 1);
    //Mat shiftedblock1 = ShiftUp(ShiftLeft(block1, -2), -2);
    //Mat shiftedblock2 = ShiftUp(ShiftLeft(block2, 2), 2);

    //double r, g, b;
    //std::tie(r, g, b) = GetCorr(shiftedblock1, shiftedblock2);
    //std::cout << r << " " << g << " " << b << std::endl;

    return 0;
}