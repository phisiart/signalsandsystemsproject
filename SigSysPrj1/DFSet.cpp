#include "DFSet.h"
#include <iostream>

disjoinset::disjoinset(int n) : s(n) {
    for (size_t i = 0; i < s.size(); ++i) {
        s[i] = -1;
    }
}

int disjoinset::find(int x) const {
    if (s[x] < 0) return x;
    return find(s[x]);
}

void disjoinset::union_set(int r1, int r2) {
    r1 = find(r1);
    r2 = find(r2);
    if (r1 == r2) return;
    if (s[r2] < s[r1]) {
        s[r1] = r2;
    } else {
        if (s[r1] == s[r2]) {
            s[r1] -= 1;
        }
        s[r2] = r1;
    }
}

std::vector<std::vector<int> > disjoinset::get_sets(size_t max_num_thresh) const {
    std::vector<std::vector<int> > ret;
    std::map<int, vector<int> > tmp;
    for (size_t i = 0; i < s.size(); ++i) {
        int j = find(i);
        tmp[j].push_back(i);
    }

    for (std::map<int, std::vector<int> >::iterator it = tmp.begin(); it != tmp.end(); ++it) {
        if (it->second.size() >= max_num_thresh) {
            ret.push_back(it->second);
        }
    }
    return ret;
}

void disjoinset::print() const {
    for (size_t i = 0; i < s.size(); ++i) {
        std::cout << i << ":\t" << s[i] << std::endl;
    }
}
