import cv2
import numpy
# from matplotlib import pyplot
# import matplotlib.pyplot as np

__author__ = 'Johnson Tan'

if __name__ == '__main__':
    src = cv2.imread('prob2.bmp')
    # edges = cv2.Canny(src, 100, 200)
    # lines = cv2.HoughLines(edges, 0.1, 0.1, 150)
    # for rho, theta in lines[0]:
    #     a = np.cos(theta)
    #     b = np.sin(theta)
    #     x0 = a*rho
    #     y0 = b*rho
    #     x1 = int(x0 + 1000 * (-b))
    #     y1 = int(y0 + 1000 * (a))
    #     x2 = int(x0 - 1000 * (-b))
    #     y2 = int(y0 - 1000 * (a))

    # cv2.line(src, (x1, y1), (x2, y2), (0, 0, 255), 1)
    # cv2.imshow('prob2', src)

    b, g, r = cv2.split(src)
    bfft = numpy.abs(numpy.fft.fft2(b))[0:20]
    # for i in range(0, 15):
    # print bfft[0][i]

    for i in range(0, 10):
        for j in range(0, 10):
            pass
            # print i, j, ':', bfft[i][j]

    gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)
    height = len(gray)
    width = len(gray[0])
    row_aves = [numpy.average(column) for column in gray]
    col_aves = [numpy.average(gray[:, row]) for row in range(0, width)]
    row_ave_fft = numpy.abs(numpy.fft.fft(row_aves))[0:20]
    col_ave_fft = numpy.abs(numpy.fft.fft(col_aves))[0:20]

    # print 'row_ave_fft:'
    for i in range(0, 20):
        pass
        # print i, ':', row_ave_fft[i]

    # print 'col_ave_fft:'
    for i in range(0, 20):
        pass
        # print col_ave_fft[i]

    row_vars = [numpy.var(column) for column in gray]
    col_vars = [numpy.var(gray[:, row]) for row in range(0, width)]
    row_fft = numpy.abs(numpy.fft.fft(row_vars))[0:20]
    col_fft = numpy.abs(numpy.fft.fft(col_vars))[0:20]

    # print 'row_fft:'
    for i in range(0, 20):
        pass
        # print i, ':', row_fft[i]

    # print 'col_fft:'
    for i in range(0, 20):
        pass
        # print col_fft[i]

    # pyplot.figure()
    # pyplot.plot(row_fft)
    # pyplot.figure()
    # pyplot.plot(col_fft, '.')

    # print bfft
    # pyplot.imshow(bfft)
    # pyplot.matshow(bfft)
    # .imshow('bluefft'. bfft)
    # cv2.imshow("src", src)
    # cv2.imshow('edges', edges)
    # cv2.waitKey()

    rown = numpy.argmax(row_fft[1:]) + 1
    coln = numpy.argmax(col_fft[1:]) + 1
    print 'There are', rown, 'rows'
    print 'There are', coln, 'columns'
    print 'There are', rown * coln, 'windows'