﻿#include<iostream>
#include<fstream>
#include<string.h>

#include"BMPFile.cpp"

using namespace std;

void SaveResult(int SpeciesNumber, int RowNumber, int ColumnNumber, int **SpeciesMatrix, char *StudentID);

int main(int argc, char *argv[]){
	
	/**************************************************/
	/*****  请将StudentID赋值为你的学号，输出结果将以你的学号命名
	/**************************************************/
	char *StudentID = "2012000000";

	cout<<StudentID<<endl;

	if(argc != 2){
		cout<<"参数错误，请将输入图片的路径作为程序的参数。"<<endl;
		return 1;
	}
	char *ImagePath = argv[1];

	//  读取图片
	BMPImage image;  // 保存图片的对象，请查看BMPFile.cpp获得更多细节
	if(!BMPFile::read(ImagePath, image)){
		cout<<"读取图片失败"<<endl;
		return 1;
	}
	//  如果需要，你也可以通过以下代码获取图片的灰度矩阵
	//  int **GrayImage;
	//  GrayImage = image.rgb2Gray();


	/**************************************************/
	/*****  你需要求出以下四个变量，并在你的代码中正确地赋值
	/**************************************************/

	int SpeciesNumber = 0;  // 图片中小精灵的 **种类** 数目。
	int ColumnNumber = 0;  // 图片中列数，即每行（横向）小精灵的个数。 
	int RowNumber = 0;  // 图片中行数，即每列（纵向）小精灵的个数。
	int **SpeciesMatrix;  // 小精灵的种类矩阵，SpeciesMatrix是一个二维数组int[RowNumber][ColumnNumber]
	
	// SpeciesMatrix[i][j]保存了第i行第j列的小精灵的 **种类**，[0][0]表示左上角，种类编号请使用1到SpeciesNumber的整数，但顺序可以任意

	// 对SpeciesMatrix的初始化可以参考如下代码
	//  SpeciesMatrix = new int*[RowNumber];
	//  for(int row = 0; row < RowNumber; row++)
	//      SpeciesMatrix[row] = new int[ColumnNumber];


	/**************************************************/
	/*********  请在下面继续写你的代码。
	/**************************************************/

	


	/**************************************************/
	/*********  请保证你的代码在此以上
	/**************************************************/


	// 保存结果前请检查：
	// (1) StudentID是正确的学号；
	// (2) SpeciesMatrix已经被初始化；
	// (3) 以下变量已被正确地赋值：SpeciesNumber, NumberInRow, NumberInColumn, SpeciesMatrix。
	
	SaveResult(SpeciesNumber, RowNumber, ColumnNumber, SpeciesMatrix, StudentID);

	cout<<"\nDone."<<endl;
	return 0;
}


//
//  请勿随意修改SaveResult()函数
//  SaveResult函数保证了输出结果的格式符合我们测试的要求
//
void SaveResult(int SpeciesNumber, int RowNumber, int ColumnNumber, int **SpeciesMatrix, char *StudentID){
	ofstream fout;
	char *filename = new char[100];
	strcpy(filename, StudentID);
	strcat(filename, ".txt");
	fout.open(filename);

	fout<<"有几种小精灵?"<<SpeciesNumber<<endl;
	fout<<"有几行小精灵?"<<RowNumber<<endl;
	fout<<"有几列小精灵?"<<ColumnNumber<<endl;
	fout<<"小精灵的位置分布:"<<endl;
	if(SpeciesMatrix != NULL){
		for(int row = 0; row< RowNumber; row++){
			for(int col=0; col < ColumnNumber; col++){
				fout<<SpeciesMatrix[row][col];
				if(col != ColumnNumber-1)
					fout<<"\t";
			}
			fout<<endl;
		}
	}

	fout.close();
}

