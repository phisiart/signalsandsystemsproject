﻿#include<iostream>
#include<fstream>

using namespace std;

//  Pixel类表示图片中的像素点。
//  包含R、G、B和Gray四个成员变量。
//  当表示彩色像素点（RGB）时，四个成员变量都有意义；
//  当表示灰度像素点时，只有Gray有意义。Pixel类的声明如下。
class Pixel{
public:
	int R, G, B;
	int Gray;
	// Default construction function
	Pixel(){
		R = 0;
		G = 0;
		B = 0;
		Gray = 0;
	}
	// Construction function for a gray pixel
	Pixel(int gray){
		R = 0;
		G = 0;
		B = 0;
		Gray = gray;
	}
	// Construction function for a rgb pixel
	Pixel(int r,int g, int b){
		R = r;
		G = g;
		B = b;
		Gray = (int)(0.2989 * R + 0.5870 * G + 0.1140 * B);
	}
};

//  BMPImage类表示bmp图片。
//  包含成员变量:
//    Pixel **Image  保存图片像素点的二维数组，[0][0]表示图片的左上角，[Height-1][Width-1]表示图片的右下角
//    int Width, Height  图片的宽和高
//    bool isGrayImage  原始图片是否是灰度
//  包含成员函数:
//    int** rgb2Gray()  返回图片的灰度值二维数组
class BMPImage{
public:
	Pixel **Image;  
	int Height, Width;
	bool isGrayImage;

	unsigned char Header[54];  // BMP header in the .bmp file

	int** rgb2Gray(){
		int row, col;
		int **gray = new int*[Height];
		for(row=0;row<Height;row++)
			gray[row] = new int [Width];

		for(row=0;row<Height;row++)
			for(col=0;col<Width;col++)
				gray[row][col] = Image[row][col].Gray;

		return gray;
	}
};


//  BMPFile类对bmp图片进行读操作。
//  只包含一个静态成员函数，从指定文件读取图片到BMPImage对象
class BMPFile{
public:
	static bool read(char *filename, BMPImage &image){
		FILE* f;
		f = fopen(filename, "rb");

		if(f == NULL){
			cout<<"[File Error] Fail to open "<<filename<<endl;
			return false;
		}

		// Read the 54-byte header
		try{
		fread(image.Header, sizeof(unsigned char), 54, f);
		}
		catch(exception e){
			return false;
		}

		// Extract image height and width from header
		image.Width = *(int*)&image.Header[18];
		image.Height = *(int*)&image.Header[22];
		
		// Color or gray image?
		short Bitcount = *(short*)&image.Header[28];
		if(Bitcount == 8)
			image.isGrayImage = true;
		else if(Bitcount == 24)
			image.isGrayImage = false;
		else{
			cout<<"[BMP Error] Please make sure .bmp file is saved as 8-bit (gray) or 24-bit (color)."<<endl;
			return false;
		}

		// Has byte offside?
		int Byteoffside = *(int*)&image.Header[10];
		Byteoffside -= 54; // The header is 54 bytes
		if(Byteoffside >0 ){
			unsigned char* buf = new unsigned char[Byteoffside];
			fread(buf, sizeof(unsigned char), Byteoffside, f);
		}

		// Initialize memory
		image.Image = new Pixel* [image.Height];
		for(int row =0; row< image.Height; row++){
			image.Image[row] = new Pixel[image.Width];
		}

		// Read the image
		if(image.isGrayImage){
			int row_padded = (image.Width + 3) & (~3);
			unsigned char* data = new unsigned char[row_padded];
			for(int row = 0; row < image.Height; row++)
			{
				fread(data, sizeof(unsigned char), row_padded, f);
				for(int col = 0; col < image.Width; col++)
					image.Image[image.Height-1-row][col] = Pixel((int)data[col]);
			}
		}
		else{
			int row_padded = (image.Width*3 + 3) & (~3);
			unsigned char* data = new unsigned char[row_padded];
			for(int row = 0; row < image.Height; row++)
			{
				fread(data, sizeof(unsigned char), row_padded, f);
				for(int col = 0; col < image.Width; col++)
				{
					// Convert (B, G, R) to (R, G, B)
					int j = col*3;
					image.Image[image.Height-1-row][col] = Pixel((int)data[j+2], (int)data[j+1], (int)data[j]);
				}
			}
		}
		fclose(f);
		return true;
	}
};
